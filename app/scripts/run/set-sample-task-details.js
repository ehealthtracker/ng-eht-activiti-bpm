(function(module){
	var RunSetSampleTaskDetails = function($rootScope){
		$rootScope.sampleTaskDetails = {
			"id": 2564,
			"name": "Handle vacation request",
			"createTime": "2016-01-17T17:37:19.590-06:00",
			"description": "rsantos would like to take 1 day(s) of vacation (Motivation: Prueba Explorer).",
			"owner": null,
			"delegationState": null,
			"dueDate": null,
			"suspended": false,
			"formProperties": [{
				"id" : "fooProperty",
				"name" : "Foo property",
				"type" : "string",
				"value" : "Foo value",
				"writable" : true,
				"customComponentField" : "foo"
			},{
				"id" : "barProperty",
				"name" : "Bar property",
				"type" : "string",
				"value" : "Bar value",
				"writable" : false,
				"customComponentField" : "bar"
			},{
				"id" : "tazProperty",
				"name" : "Taz property",
				"type" : "string",
				"value" : null,
				"writable" : true,
				"customComponentField" : "taz"
			},{
				"id": "vacationApproved",
				"name": "Do you approve this vacation",
				"type": "enum",
				"value": null,
				"readable": true,
				"writable": true,
				"required": true,
				"datePattern": null,
				"enumValues": [{
					"id": "true",
					"name": "Approve"
				}, {
					"id": "false",
					"name": "Reject"
				}]
			}, {
				"id": "managerMotivation",
				"name": "Motivation",
				"type": "string",
				"value": null,
				"readable": true,
				"writable": true,
				"required": false,
				"datePattern": null,
				"enumValues": []
			}]
		};
	};
	RunSetSampleTaskDetails.$inject = ['$rootScope'];
	module.run(RunSetSampleTaskDetails);
})(angular.module('ngEhtActivitiBpmApp'));