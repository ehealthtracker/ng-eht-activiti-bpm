(function(module){
	var RunSetRouteService = function($rootScope, $state){
		$rootScope.$state = $state;
	};
	RunSetRouteService.$inject = ['$rootScope', '$state'];
	module.run(RunSetRouteService);
})(angular.module('ngEhtActivitiBpmApp'));