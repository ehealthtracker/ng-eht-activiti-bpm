'use strict';

(function(module){
	var TaskCtrl = function($scope, EhtTaskService){
		$scope.claimableTasks = [];
		$scope.assignedTasks = [];
		$scope.task = null;
		$scope.isLoadingClaimableTasks = false;
		$scope.isLoadingAssignedTasks = false;
		$scope.isLoadingTask = false;
		$scope.isClaimingTask = false;
		$scope.isRevokingTask = false;
		$scope.isCompletingTask = false;
		$scope.taskId = 5200;

		$scope.getClaimableTasks = function(){
			$scope.isLoadingclaimableTasks = true;

			EhtTaskService.getAllClaimableTasks().then(function(successResponse){
				$scope.claimableTasks = successResponse.data;
				$scope.isLoadingclaimableTasks = false;
			});
		};

		$scope.getAssignedTasks = function(){
			$scope.isLoadingAssignedTasks = true;

			EhtTaskService.getAllAssignedTasks().then(function(successResponse){
				$scope.assignedTasks = successResponse.data;
				$scope.isLoadingAssignedTasks = false;
			});
		};

		$scope.getTask = function(taskId){
			$scope.isLoadingTask = true;

			EhtTaskService.getTaskById(taskId).then(function(successResponse){
				$scope.task = successResponse.data;
				$scope.isLoadingTask = false;
			});
		};

		$scope.claim = function(taskId){
			$scope.isClaimingTask = true;

			EhtTaskService.claimTask(taskId).then(
				function(successResponse){
					$scope.claimed = true;
				},
				function(errorResponse){
					$scope.claimError = errorResponse.status;
				}).finally(function() {
					$scope.isClaimingTask = false;
				});
		};

		$scope.revoke = function(taskId){
			$scope.isRevokingTask = true;

			EhtTaskService.revokeTask(taskId).then(
				function(successResponse){
					$scope.revoked = true;
				},
				function(errorResponse){
					$scope.revokeError = errorResponse.status;
				}).finally(function() {
					$scope.isRevokingTask = false;
				});
		};

		$scope.complete = function(taskId){
			$scope.isCompletingTask = true;

			EhtTaskService.completeTask(taskId).then(
				function(successResponse){
					$scope.completed = true;
				},
				function(errorResponse){
					$scope.completeError = errorResponse.status;
				}).finally(function() {
					$scope.isCompletingTask = false;
				});
		};
	};

	TaskCtrl.$inject = ['$scope', 'EhtTaskService'];
	module.controller('TaskCtrl', TaskCtrl);

})(angular.module('ngEhtActivitiBpmApp'));