'use strict';

/**
 * @ngdoc function
 * @name ngEhtActivitiBpmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ngEhtActivitiBpmApp
 */
angular.module('ngEhtActivitiBpmApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    console.log('Main controller');
  });
