'use strict';

(function(module){
	var TaskListCtrl = function($scope, EhtTaskService){
		$scope.getClaimableTasks = function(){
			EhtTaskService.getAllClaimableTasks().then(function(successResponse){
				$scope.claimableTasks = successResponse.data;
			});
		};
		$scope.getAssignedTasks = function(){
			EhtTaskService.getAllAssignedTasks().then(function(successResponse){
				$scope.assignedTasks = successResponse.data;
			});
		};

		$scope.showingTaskList = false;

		$scope.goClaimableTask = function(task){
			$scope.showingTaskList = false;
			$scope.taskList = $scope.claimableTasks;
			$scope.selectedTaskId = task.id;
			$scope.tasksState = 'claimable';

			$scope.showingTaskList = true;
		};

		$scope.goAllClaimableTasks = function(){
			$scope.showingTaskList = false;
			$scope.taskList = $scope.claimableTasks;
			$scope.selectedTaskId = null;
			$scope.tasksState = 'claimable';
			$scope.showingTaskList = true;
		};

		$scope.goAssignedTask = function(task){
			$scope.showingTaskList = false;
			$scope.taskList = $scope.assignedTasks;
			$scope.selectedTaskId = task.id;
			$scope.tasksState = 'assigned';
			$scope.showingTaskList = true;
		};

		$scope.goAllAssignedTasks = function(){
			$scope.showingTaskList = false;
			$scope.taskList = $scope.assignedTasks;
			$scope.selectedTaskId = null;
			$scope.tasksState = 'assigned';
			$scope.showingTaskList = true;
		};
	};
	TaskListCtrl.$inject = ['$scope', 'EhtTaskService'];
	module.controller('TaskListCtrl', TaskListCtrl);
})(angular.module('ngEhtActivitiBpmApp'));