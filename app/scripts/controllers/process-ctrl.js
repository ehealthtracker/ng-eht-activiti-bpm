(function(module){
	var ProcessCtrl = function($scope, EhtProcessService){
		$scope.processDefinition = null;
		$scope.processDefinitions = [];
		$scope.isLoadingProcessDefinitions = false;
		$scope.isCreatingInstance = false;
		$scope.processDefinitionId = "vacationRequest:1:39";

		$scope.getProcessDefinition = function(processDefinitionId){
			$scope.isLoadingProcessDefinitions = true;

			EhtProcessService.getProcessDefinitionById(processDefinitionId).then(
				function(successResponse){
					$scope.processDefinition = successResponse.data;
				},
				function(errorResponse){
					$scope.processDefStatus = errorResponse.status;
				}).finally(function() {
					$scope.isLoadingProcessDefinitions = false;
				});
		};
	};

	ProcessCtrl.$inject = ['$scope', 'EhtProcessService'];
	module.controller('ProcessCtrl', ProcessCtrl);
})(angular.module('ngEhtActivitiBpmApp'));