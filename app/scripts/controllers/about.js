'use strict';

/**
 * @ngdoc function
 * @name ngEhtActivitiBpmApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ngEhtActivitiBpmApp
 */
angular.module('ngEhtActivitiBpmApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
