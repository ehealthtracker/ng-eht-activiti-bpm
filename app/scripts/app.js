'use strict';

/**
 * @ngdoc overview
 * @name ngEhtActivitiBpmApp
 * @description
 * # ngEhtActivitiBpmApp
 *
 * Main module of the application.
 */
angular
  .module('ngEhtActivitiBpmApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngEhtBpm',
    'ui.router'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('main', {
      url: '/',
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    });
    $stateProvider.state('about', {
      url: '/about',
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl'
    });
    $stateProvider.state('task', {
      url: '/task',
      templateUrl: 'views/task.html',
      controller: 'TaskCtrl'
    });
    $stateProvider.state('process', {
      url: '/process',
      templateUrl: 'views/process.html',
      controller: 'ProcessCtrl'
    });
    $stateProvider.state('task-list', {
      url: '/task-list',
      templateUrl: '/views/task-list.html',
      controller: 'TaskListCtrl'
    });
    $urlRouterProvider.otherwise('/');
  });
