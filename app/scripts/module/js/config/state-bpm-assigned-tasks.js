(function(module){
	var BpmAssignedTasksCtrl = function(){

	};
	BpmAssignedTasksCtrl.$inject = [];

	var ConfigStateBpmAssignedTasks = function($stateProvider){
		$stateProvider.state('assigned-tasks', {
			'url' : '/bpm/assigned-tasks',
			'controller' : BpmAssignedTasksCtrl,
			'templateProvider' : ['$templateCache', function($templateCache){
				return $templateCache.get('assigned-tasks.html');
			}]
		});
	};
	ConfigStateBpmAssignedTasks.$inject = ['$stateProvider'];

	module.config(ConfigStateBpmAssignedTasks)
})(angular.module('ngEhtBpm'));