(function(module){
	var BpmClaimableTasksCtrl = function(){

	};
	BpmClaimableTasksCtrl.$inject = [];

	var ConfigStateBpmClaimableTasks = function($stateProvider){
		$stateProvider.state('claimable-tasks', {
			'url' : '/bpm/claimable-tasks',
			'controller' : BpmClaimableTasksCtrl,
			'templateProvider' : ['$templateCache', function($templateCache){
				return $templateCache.get('claimable-tasks.html');
			}]
		});
	};
	ConfigStateBpmClaimableTasks.$inject = ['$stateProvider'];

	module.config(ConfigStateBpmClaimableTasks)
})(angular.module('ngEhtBpm'));