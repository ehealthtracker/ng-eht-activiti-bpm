(function(module){
	var EhtBpmFormDataService = function($http){
		var getBaseHeaders = function(){
			var headers = {
				'Content-Type' : 'application/json',
				'Accept' : 'application/json'
			};
			return headers;
		};

		var submitTaskForm = function(taskId, properties){
			var headers = getBaseHeaders();

			var data = {
				'taskId' : taskId,
				'properties' : properties
			};

			return $http({
				'url' : '/ehealthtracker-rest/rest/bpm/form-data',
				'method' : 'POST',
				'headers' : headers,
				'data' : data
			});
		};


		var service = {};
		/**
		*	Submit form data to complete a task		
		*	
		*	@param taskId - task id
		*	@param properties - an array of property elements with id and name attributes
		*/
		service.submitTaskForm = submitTaskForm;
		
		return service;
	};
	EhtBpmFormDataService.$inject = ['$http'];
	module.factory('EhtBpmFormDataService', EhtBpmFormDataService);
})(angular.module('ngEhtBpm'));