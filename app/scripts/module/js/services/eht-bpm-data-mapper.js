(function(module){
	var EhtBpmDataMapper = function($parse, $rootScope, EhtBpmFormDataService, $state){
		var completeTask = function(dynamicControlsEvents, taskDetails, onTaskComplete){
			var areDinamycControlsValid = dynamicControlsEvents.validateDynamicControls();
			if(!areDinamycControlsValid){
				console.log('Dynamic controls not valid.');
				return;
			}

			var props = getRequestVariables(taskDetails);
			EhtBpmFormDataService.submitTaskForm(taskDetails.id, props).then(function(successResponse){
				if(onTaskComplete) onTaskComplete();
				else
					$state.go('assigned-tasks');
			});
		};
		var updateComponentAndCompleteTask = function(dynamicControlsEvents, taskDetails, updateComponentCallback){
			var areDinamycControlsValid = dynamicControlsEvents.validateDynamicControls();
			if(!areDinamycControlsValid){
				console.log('Dynamic controls not valid.');
				return;
			}
			console.log('Dynamic controls valid.');
			var taskFormDataToSubmit = getRequestVariables(taskDetails);
			console.log(' - task form data to submit: ' + angular.toJson(taskFormDataToSubmit));

			var tasKHeader = {
				'properties' : taskFormDataToSubmit,
				'taskId' : taskDetails.id
			};
			var jsonTaskHeader = angular.toJson(tasKHeader);
			console.log(' - task header: ' + jsonTaskHeader);

			updateComponentCallback(jsonTaskHeader);
		};
		var getRequestVariables = function(taskDetails){
			var requestVariables = [];
			for(var index in taskDetails.formProperties){
				var formProperty = taskDetails.formProperties[index];
				if(!formProperty.writable) continue;
				var requestVariable = {
					'id' : formProperty.id,
					'value' : formProperty.value
				};
				requestVariables.push(requestVariable);
			}
			return requestVariables;
		};

		var setPropertyValueOnField = function(formProperty, scope, expression){
			var setter = $parse(expression).assign;
			setter(scope, formProperty.value);
		};

		var setWatchOnField = function(formProperty, scope, expression){
			scope.$watch(expression, function(newValue, oldValue){
				formProperty.value = newValue;
			});
		};

		var bindPropertiesToFields = function(taskDetails, scope, mappings){
			if(!taskDetails.formProperties) return;

			for(var formPropertyIndex in taskDetails.formProperties){
				var formProperty = taskDetails.formProperties[formPropertyIndex];
				if(!formProperty.customComponentField) continue;

				var expression = mappings[formProperty.customComponentField];
				if(!expression) continue;

				if(formProperty.value && formProperty.value != null)
					setPropertyValueOnField(formProperty, scope, expression);

				if(formProperty.writable)
					setWatchOnField(formProperty, scope, expression);
			}
		};

		var emitCustomComponentEvent = function(taskDetails, customComponentMetadata){
			var eventName = 'custom-component-' + customComponentMetadata.customComponent;
			$rootScope.$emit(eventName, taskDetails);
		};

		var getCustomComponentFieldMapping = function(taskDetails, taskFormProperty){
			var customComponentMetadata = null;
			for(var taskFormPropertyIndex in taskDetails.formProperties){
				var auxTaskFormProperty = taskDetails.formProperties[taskFormPropertyIndex];
				if(auxTaskFormProperty.id == 'customComponentMetadata'){
					customComponentMetadata = auxTaskFormProperty;
					break;
				}
			}
			if(!customComponentMetadata) return null;
			
			var customComponentMetadataValue = angular.fromJson(customComponentMetadata.value);
			var mapping = null;
			for(var mappingIndex in customComponentMetadataValue.mappings){
				var auxMapping = customComponentMetadataValue.mappings[mappingIndex];
				if(auxMapping.formProperty == taskFormProperty.id){
					mapping = auxMapping;
					break;
				}
			}
			return mapping;
		};

		var isCustomComponentField = function(taskDetails, taskFormProperty){
			var customComponentFieldMapping = 
				getCustomComponentFieldMapping(taskDetails, taskFormProperty);

			return customComponentFieldMapping != null;
		};

		var getTaskFormPropertyById = function(id, taskDetails){
			var target = null;
			for(var taskFormPropertyIndex in taskDetails.formProperties){
				var auxTaskFormProperty = taskDetails.formProperties[taskFormPropertyIndex];
				if(auxTaskFormProperty.id == id){
					target = auxTaskFormProperty;
					break;
				}
			} 
			return target;
		};

		var service = {};
		/**
		*	Receive a task details object and extract the form properties
		*	so it parse them to request variables 
		*
		*	@param taskDetails
		*	@return Request variables
		*/
		service.getRequestVariables = getRequestVariables;
		/**
		*	Binds task details form properties to custom component fields
		*	The binding consists in:
		*	1. Register watch functions on custom component's fields through the custom component scope
		*		for writable properties so any change con fields reflects in propertie's values
		*	2. Calling setter functions on custom component's fields to initialize their values with
		*		values coming from properties
		*
		*	@param taskDetails
		*	@param scope - custom component scope
		*	@param mappings - hash containing custom component mappings between
		*		logical field names and scope names
 		*/
		service.bindPropertiesToFields = bindPropertiesToFields;
		/**
		*	Emits custom component event on $rootScope in the form custom-component-{{taskDetails.customComponent}}
		*	
		*	@param taskDetails
		*/
		service.emitCustomComponentEvent = emitCustomComponentEvent;
		/**
		*	Extract custom component field mapping info from task details related to a task from property
		*	
		*	@param taskDetails 
		*	@param taskFromProperty
		*	@return - custom component field mapping object {customComponentField, formProperty}
		*/
		service.getCustomComponentFieldMapping = getCustomComponentFieldMapping;
		/**
		*	Determines if a task from property has custom component field mapping info in task details
		*	
		*	@param taskDetails
		*	@param taskFromProperty
		*	@return - true if it does, false if it doesn´t
		*/
		service.isCustomComponentField = isCustomComponentField;
		/**
		*	Extract task form property from task details by id
		*
		*	@param id - task form property id to search
		*	@param taskDetails
		*	@return task form property found or null.
		*/
		service.getTaskFormPropertyById = getTaskFormPropertyById;
		service.updateComponentAndCompleteTask = updateComponentAndCompleteTask;
		service.completeTask = completeTask;
		return service;
	};
	EhtBpmDataMapper.$inject = ['$parse', '$rootScope', 'EhtBpmFormDataService', '$state'];
	module.factory('EhtBpmDataMapper', EhtBpmDataMapper);
})(angular.module('ngEhtBpm'));