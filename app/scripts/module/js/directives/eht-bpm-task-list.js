(function(module){
	var EhtBpmTaskListDirective = function($templateCache, EhtTaskService, EhtBpmDataMapper, $rootScope, $location, EhtBpmFormDataService){
		return {
			'restrict' : 'A',
			'template' : $templateCache.get('task-list.html'),
			'scope' : {
				'tasksState' : '=?', // one of: {claimable, assigned} defaults to assigned 
				'taskList' : '=?', //list of tasks metadata, if not passed its gonna be loaded
				'selectedTaskId' : '=?', //Optional (if not taskList passed is ignored), id of the selected task ,
				'labelCreateTime' : '=?',
				'labelDueDate' : '=?',
				'labelClaim' : '=?',
				'labelClaiming' : '=?',
				'labelComplete' : '=?',
				'labelCompleting' : '=?',
				'labelRevoke' : '=?',
				'labelRevoking' : '=?',
				'labelRequiredField' : '=?' 
			},
			'link' : function($scope, element, attrs){		

				if(!$scope.labelCreateTime) $scope.labelCreateTime = 'create time';
				if(!$scope.labelDueDate) $scope.labelDueDate = 'due date';
				if(!$scope.labelClaim) $scope.labelClaim = 'claim';
				if(!$scope.labelClaiming) $scope.labelClaiming = 'claiming';
				if(!$scope.labelComplete) $scope.labelComplete = 'complete';
				if(!$scope.labelCompleting) $scope.labelCompleting = 'completing';
				if(!$scope.labelRevoke) $scope.labelRevoke = 'revoke';
				if(!$scope.labelRevoking) $scope.labelRevoking = 'revoking';
				if(!$scope.labelRequiredField) $scope.labelRequiredField = 'required field';

				$scope.claiming = false;
				$scope.revoking = false;
				$scope.completing = false;
				$scope.events = {};

				$scope.isLoadingTaskList = false;
				$scope.isLoadingTaskDetails = true;
				if(!$scope.tasksState)
					$scope.tasksState = 'assigned';

				$scope.loadTaskList = function(){
					$scope.isLoadingTaskList = true;
					var listPromise = null;

					listPromise = $scope.tasksState == 'assigned' ?
						EhtTaskService.getAllAssignedTasks() :
						EhtTaskService.getAllClaimableTasks();

					listPromise.then(function(successResponse){
						$scope.taskList = successResponse.data;
					}).finally(function(){
						$scope.isLoadingTaskList = false;
					});
				};

				if(!$scope.taskList){
					$scope.selectedTaskId = null;
					$scope.isLoadingTaskDetails = false;
					$scope.loadTaskList();
				}

				$scope.loadTaskDetails = function(){
					$scope.isLoadingTaskDetails = true;
					EhtTaskService.getTaskById($scope.selectedTaskId).then(function(successResponse){
						$scope.isLoadingTaskDetails = false;
						$scope.taskDetails = successResponse.data;						
					});
				};

				if($scope.selectedTaskId){
					$scope.loadTaskDetails();
				};

				$scope.taskSelected = function(task){
					if(task.id == $scope.selectedTaskId) return;
					$scope.selectedTaskId = task.id;
					$scope.loadTaskDetails();
				};

				$scope.claim = function(taskDetails){
					$scope.claiming = true;
					EhtTaskService.claimTask(taskDetails.id).then(function(successResponse){
						$scope.removeSelectedTaskFromTaskList();
					}).finally(function(){
						$scope.claiming = false;
					});
				};

				$scope.revoke = function(taskDetails){
					$scope.revoking = true;
					EhtTaskService.revokeTask(taskDetails.id).then(function(successResponse){
						$scope.removeSelectedTaskFromTaskList();
					}).finally(function(){
						$scope.revoking = false;
					});
				};



				$scope.complete = function(taskDetails){
					$scope.completing = true;
					var customComponentMetadataFormProperty = 
						EhtBpmDataMapper.getTaskFormPropertyById('customComponentMetadata', taskDetails);
					if(customComponentMetadataFormProperty){
						var customComponentMetadata = angular.fromJson(customComponentMetadataFormProperty.value);
						if($rootScope.appMetadata.appName == customComponentMetadata.customComponentApp){ // In this app
							EhtBpmDataMapper.emitCustomComponentEvent(taskDetails, customComponentMetadata);
						}else{
							window.location = '/' + taskDetails.customApp + '#/bpm/task/' + taskDetails.id;
						}
						
						return;
					}


					var areValidDynamicControls = $scope.events.validateDynamicControls();
					if(!areValidDynamicControls){
						$scope.completing = false;	
						return;					
					};
					var props = EhtBpmDataMapper.getRequestVariables(taskDetails);

					EhtBpmFormDataService.submitTaskForm(taskDetails.id, props).then(function(successResponse){
						$scope.removeSelectedTaskFromTaskList();
					}).finally(function(){
						$scope.completing = false;
					});
				};

				$scope.removeSelectedTaskFromTaskList = function(){
					var taskDetails = $scope.taskDetails;
					for(var index in $scope.taskList.items){
						var currentTask = $scope.taskList.items[index];
						if(currentTask.id == taskDetails.id){
							$scope.selectedTaskId = null;
							$scope.taskList.items.splice(index, 1);
							break;
						}
					}
				};
			}
		};
	};
	EhtBpmTaskListDirective.$inject = ['$templateCache', 'EhtTaskService', 'EhtBpmDataMapper', '$rootScope', '$location', 'EhtBpmFormDataService'];
	module.directive('ehtBpmTaskList', EhtBpmTaskListDirective);
})(angular.module('ngEhtBpm'));