(function(module){
	var RunSetTaskListTemplate = function($templateCache){
		$templateCache.put('task-list.html', 
			'<div class="row">' +
				'<div class="col-sm-4">' +
					'<div ng-show="isLoadingTaskList" class="alert alert-info">' +
						'Loading tasks' +
					'</div>' +
					'<div class="list-group" ng-show="!isLoadingTaskList && taskList.items.length">' +
						'<a href="javascript:void(0)" ' + 
								'class="list-group-item" ' + 
								'ng-repeat="task in taskList.items" ' + 
								'ng-click="taskSelected(task)" ' +
								'ng-class="{ \'active\' : task.id == selectedTaskId}">' +
							'<p class="list-group-item-heading"><strong>{{task.name}}</strong> (Id: {{task.id}})</p>' +
							'<p class="list-group-item-text">{{task.createTime}}</p>' +	
						'</a>' +
					'</div>' + 
					'<div ng-if="!isLoadingTaskList && !taskList.items.length" class="alert alert-info">No tasks found</div>' +
				'</div>' +
				'<div class="col-sm-8">' +
					'<div ng-show="!selectedTaskId" class="alert alert-info">' +
						'No task selected' +
					'</div>' +
					'<div ng-show="selectedTaskId">' + 
						'<div ng-show="isLoadingTaskDetails">' +
							'Loading task details' +
						'</div>' +
						'<div ng-show="!isLoadingTaskDetails" class="panel panel-primary">' +
							'<div class="panel-heading">' +
								'<h4 class="pull-left">{{taskDetails.name}}</h4>' +
								'<span class="pull-right">Id: {{taskDetails.id}}</span>' +
								'<div class="clearfix"/>' +
							'</div>' +
							'<div class="panel-body">' +
								'<div class="row">' +
									'<div class="col-sm-12">' +
										'<p>{{taskDetails.description}}</p>' +
									'</div>' +
									'<div class="col-sm-12">' +
										'<div class="form-horizontal">' + 
											'<div class="form-group">' +
												'<label class="control-label col-sm-3">{{labelCreateTime}}</label>' +
												'<div class="col-sm-9">' + 
													'<input ng-model="taskDetails.createTime" class="form-control" readonly/>' +
												'</div>' + 
											'</div>' +
											'<div class="form-group">' + 
												'<label class="control-label col-sm-3">{{labelDueDate}}</label>' +
												'<div class="col-sm-9">' +
													'<input ng-model="taskDetails.dueDate" class="form-control" readonly/>' +
												'</div>' +
											'</div>' +
										'</div>' +
										'<div data-eht-bpm-task-dynamic-controls ' +
												'data-events="events" ' +
												'data-readonly="taskDetails.customComponent || tasksState == \'claimable\'" ' + 
												'data-task="taskDetails">' + 
										'</div>' +
										'<div class="form-horizontal">' +
											'<div class="form-group">' +
												'<div class="col-sm-9 col-sm-offset-3">' +
													'<button ng-if="tasksState == \'claimable\'" ' +
															'ng-click="claim(taskDetails)" ' + 
															'ng-disabled="claiming" ' +
															'class="btn btn-primary">' +
														'<span ng-if="!claiming">{{labelClaim}}</span>' +
														'<span ng-if="claiming">{{labelClaiming}}</span>' +
													'</button>' +
													'<button ng-if="tasksState == \'assigned\'" ' + 
															'ng-click="revoke(taskDetails)" ' +
															'ng-disabled="revoking || completing" ' +
															'class="btn btn-danger">' +
														'<span ng-if="!revoking">{{labelRevoke}}</span>' +
														'<span ng-if="revoking">{{labelRevoking}}</span>' +
													'</button>' +
													'<button ng-if="tasksState == \'assigned\'" ' +
															'ng-click="complete(taskDetails)" ' +
															'ng-disabled="completing || revoking" ' +
															'class="btn btn-success" style="margin-left:10px;">' +
														'<span ng-if="!completing">{{labelComplete}}</span>' +
														'<span ng-if="completing">{{labelCompleting}}</span>' +
													'</button>' +
												'</div>' +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +								
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>'
		);
	};
	RunSetTaskListTemplate.$inject = ['$templateCache'];
	module.run(RunSetTaskListTemplate);
})(angular.module('ngEhtBpm'));