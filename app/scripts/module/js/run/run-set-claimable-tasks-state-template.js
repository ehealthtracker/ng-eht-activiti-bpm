(function(module){
	var RunSetClaimableTasksStateTemplate = function($templateCache){
		$templateCache.put('claimable-tasks.html',
			'<div>' +
				'<div data-eht-bpm-task-list data-tasks-state="\'claimable\'"/>' +
			'</div>');
	};
	RunSetClaimableTasksStateTemplate.$inject = ['$templateCache'];
	module.run(RunSetClaimableTasksStateTemplate);
})(angular.module('ngEhtBpm'));