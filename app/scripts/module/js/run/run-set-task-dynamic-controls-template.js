(function(module){
	var RunSetTaskDynamicControlsTemplate = function($templateCache){
		$templateCache.put('task-dynamic-controls.html',
			'<div class="row"><div class="col-sm-12">' +
			'<form name="dynamicControlsForm" class="form-horizontal">' +
				'<div class="form-group" ' +
						'ng-if="!isTaskPropertyMappedOrMetadata(task, taskProperty)" ' +
						'ng-repeat="taskProperty in task.formProperties | filter : { writable : false}">' +
					'<label class="col-sm-3 control-label">{{taskProperty.name}}</label>' +
					'<div class="col-sm-9">' +
						'<input class="form-control" ' +
								'placeholder="{{taskProperty.name}}" ' +
								'value="{{taskProperty.value}}" ' +
								'readonly/>' +
					'</div>' +
				'</div>' +
				'<div class="form-group" ' +
						'ng-if="!isTaskPropertyMappedOrMetadata(task, taskProperty)" ' +
						'ng-class="{ \'has-error\' : (dynamicControlsForm.$submitted || dynamicControlsForm[taskProperty.id].$dirty) && dynamicControlsForm[taskProperty.id].$invalid }" ' +
						'ng-repeat="taskProperty in task.formProperties | filter : { writable : true}">' +
					'<label class="col-sm-3 control-label">{{taskProperty.name}}</label>' +
					'<div class="col-sm-9">' +
						'<input name="{{taskProperty.id}}" ' +
								'ng-if="taskProperty.type != \'enum\'" ' + 
								'ng-readonly="readonly" ' +
								'ng-required="taskProperty.required" ' +
								'name="{{taskProperty.name}}" ' +
								'class="form-control" ' +
								'placeholder="{{taskProperty.name}}" ' +
								'ng-model="taskProperty.value"/>' +
						'<select name="{{taskProperty.id}}" ' +
								'ng-if="taskProperty.type == \'enum\'" ' +
								'data-required="taskProperty.required" ' +
								'ng-readonly="readonly" ' +
								'class="form-control" ' +
								'ng-model="taskProperty.value" ' +
								'ng-options="enumValue.id as enumValue.name for enumValue in taskProperty.enumValues">' +
						'</select>' +
						'<div ng-if="(dynamicControlsForm.$submitted || dynamicControlsForm[taskProperty.id].$dirty) && dynamicControlsForm[taskProperty.id].$error.required" ' +
								'class="help-block"> ' +
							'{{labelRequiredField}}' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</form>' +
			'</div></div>'
		);
	};
	RunSetTaskDynamicControlsTemplate.$inject = ['$templateCache'];
	module.run(RunSetTaskDynamicControlsTemplate);
})(angular.module('ngEhtBpm'));