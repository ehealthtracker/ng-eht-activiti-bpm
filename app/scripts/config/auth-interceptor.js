(function(module){
	var AuthInteceptor = function($rootScope){
		return {
			'request' : function(config){
				config.headers['Eht-Token'] = $rootScope.principal.ehtToken;
				return config;
			}
		};
	};
	AuthInteceptor.$inject = ['$rootScope'];
	module.factory('AuthInteceptor', AuthInteceptor);

	var ConfigAuthInterceptor = function($httpProvider){
		$httpProvider.interceptors.push('AuthInteceptor');
	};
	ConfigAuthInterceptor.$inject = ['$httpProvider'];
	module.config(ConfigAuthInterceptor);

	var RunDefaultEhtToken = function($rootScope){
		if(!$rootScope.principal) $rootScope.principal = {};
		$rootScope.principal.ehtToken = '4d0001d6-0edb-4fa6-9aca-e7500889f298';
	};
	RunDefaultEhtToken.$inject = ['$rootScope'];
	module.run(RunDefaultEhtToken);
})(angular.module('ngEhtActivitiBpmApp'));

