(function(module){
	var SampleCustomComponentCtrl = function($scope, $stateParams, EhtBpmDataMapper){
		$scope.taskDetails = $stateParams.taskDetails;
		$scope.dynamicControlsEvents = {};

		var mappings = {
			'foo' : 'complex.foo',
			'bar' : 'complex.bar',
			'taz' : 'taz',
			'buzz' : 'buzz'
		};

		if($scope.taskDetails)
			EhtBpmDataMapper.bindPropertiesToFields($scope.taskDetails, $scope, mappings);
	};
	SampleCustomComponentCtrl.$inject = ['$scope', '$stateParams', 'EhtBpmDataMapper'];

	var ConfigStateSampleCustomComponent = function($stateProvider){
		$stateProvider.state('sample-custom-component',{
			'url' : '/sample-custom-component',
			'params' : {
				'taskDetails' : null
			},
			'controller' : SampleCustomComponentCtrl,
			'templateUrl' : '/views/sample-custom-component.html'
		});
	};
	ConfigStateSampleCustomComponent.$inject = ['$stateProvider'];
	module.config(ConfigStateSampleCustomComponent);
})(angular.module('ngEhtActivitiBpmApp'));