'use strict';

(function(module){
	var EhtProcessService = function($http){
		var getHeaders = function(){
			var headers = {
				'Content-Type' : 'application/json',
				'Accept' : 'application/json'
			};
			return headers;
		};

		return {
			'getProcessDefinitionById' : function(processDefinitionId){
				return $http({
					'url' : '/ehealthtracker-rest/rest/bpm/process-definition/' + processDefinitionId,
					'method' : 'GET',
					'headers' : getHeaders()
				});
			},
			'createProcessInstance' : function(request){
				return $http({
					'url' : '/ehealthtracker-rest/rest/bpm/process-instance',
					'method' : 'POST',
					'headers' : getHeaders(),
					'data' : request
				});
			}
		}
	};

	EhtProcessService.$inject = ['$http'];
	module.factory('EhtProcessService', EhtProcessService);
})(angular.module('ngEhtBpm'));