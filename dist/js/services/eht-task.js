'use strict';

(function(module){
	var EhtTaskService = function($http){
		var getHeaders = function(){
			var headers = {
				'Content-Type' : 'application/json',
				'Accept' : 'application/json'
			};
			return headers;
		};

		/**
		*	@param path.- Subpath to append to '/ehealthtracker-rest/rest/bpm/task',
		*/
		var getSimpleTask = function(path){
			var headers = getHeaders();
			return $http({
				'url' : '/ehealthtracker-rest/rest/bpm/task' + path,
				'method' : 'GET',
				'headers' : headers
			});
		};

		var getSimplePostTask = function(path, requestVariables){
			var headers = getHeaders();
			var requestConfig = {
				'url' : '/ehealthtracker-rest/rest/bpm/task/' + path,
				'method' : 'POST',
				'headers' : headers
			};

			if(requestVariables){
				requestConfig.data = requestVariables;
			}

			return $http(requestConfig);
		};
		
		return {
			'getTaskById' : function(taskId){
				return getSimpleTask('/' + taskId);
			},
			'getAllClaimableTasks' : function(){
				return getSimpleTask('/claimable');
			},
			'getAllAssignedTasks' : function(){
				return getSimpleTask('/assigned');
			},
			'claimTask' : function(taskId){
				return getSimplePostTask(taskId + '/claim');
			},
			'revokeTask' : function(taskId){
				return getSimplePostTask(taskId + '/revoke');
			},
			'completeTask' : function(taskId, requestVariables){
				return getSimplePostTask(taskId + '/complete', requestVariables);
			}
		};
	};

	EhtTaskService.$inject = ['$http'];
	module.factory('EhtTaskService', EhtTaskService);
})(angular.module('ngEhtBpm'));