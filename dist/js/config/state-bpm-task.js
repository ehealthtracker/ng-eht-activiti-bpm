(function(module){
	var BpmTaskController = function(EhtTaskService, $stateParams, $rootScope, EhtBpmDataMapper){
		EhtTaskService.getTaskById($stateParams.taskId).then(function(successResponse){
			var taskDetails = successResponse.data;
			EhtBpmDataMapper.emitCustomComponentEvent(taskDetails);
		});
	};
	BpmTaskController.$inject = ['EhtTaskService', '$stateParams', '$rootScope', 'EhtBpmDataMapper'];
	var ConfigStateBpmTask = function($stateProvider){
		$stateProvider.state('bpm-task', {
			'url' : '/bpm/task/:taskId',
			'controller' : BpmTaskController,
			'template' : "<div></div>"
		});
	};
	ConfigStateBpmTask.$inject = ['$stateProvider'];
	module.config(ConfigStateBpmTask);
})(angular.module('ngEhtBpm'));