(function(module){
	var RunSetAssignedTasksStateTemplate = function($templateCache){
		$templateCache.put('assigned-tasks.html',
			'<div>' +
				'<div data-eht-bpm-task-list data-tasks-state="\'assigned\'"/>' +
			'</div>');
	};
	RunSetAssignedTasksStateTemplate.$inject = ['$templateCache'];
	module.run(RunSetAssignedTasksStateTemplate);
})(angular.module('ngEhtBpm'));