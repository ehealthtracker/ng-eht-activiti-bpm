(function(module){
	var EhtBpmTaskDynamicControlsDirective = function($templateCache, $timeout, EhtBpmDataMapper){
		return {
			'restrict' : 'A',
			'template' : $templateCache.get('task-dynamic-controls.html'),
			'scope' : {
				'task' : '=?',
				'readonly' : '=?',
				'events' : '=?',
				'labelRequiredField' : '=?'
			},
			'link' : function($scope, element, attrs){
				$scope.isTaskPropertyMappedOrMetadata = function(task, taskProperty){
					return taskProperty.id == 'customComponentMetadata' || 
						EhtBpmDataMapper.isCustomComponentField(task, taskProperty);
				};
				$timeout(function(){
					if(!$scope.labelRequiredField) $scope.labelRequiredField = 'required field';
					$scope.events.validateDynamicControls = function(){
						$scope.dynamicControlsForm.$setSubmitted();
						return $scope.dynamicControlsForm.$valid;
					};	
				});				
			}
		};
	};
	EhtBpmTaskDynamicControlsDirective.$inject = ['$templateCache', '$timeout', 'EhtBpmDataMapper'];
	module.directive('ehtBpmTaskDynamicControls', EhtBpmTaskDynamicControlsDirective);
})(angular.module('ngEhtBpm'));